<?php

require __DIR__ . '/../config.php';
require __DIR__ . '/PhoneUtils.php';

$utils = new PhoneUtils($arrConfig);
$rawData = $utils->getPhoneNumbers();
$phoneNumbers = $utils->formatPhoneNumbers($rawData);
if (PHP_SAPI == 'cli') {
    echo implode(PHP_EOL, $phoneNumbers);
} else {
    echo $utils->buildHtmlTable($phoneNumbers);
}
