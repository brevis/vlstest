<?php

/**
 * Various functions for Task 1 wrapped in one class
 */
class PhoneUtils {

    const PHONE_MAX_LENGTH = 15; // regarding E.164
    const PHONE_MIN_LENGTH = 5; // unfortunately in E.164 no exact info about min length, so let's set it to 5

    /** @var PDO */
    protected $dbh;

    public function __construct($config)
    {
        $dns = "mysql:host={$config['dbhost']};dbname={$config['dbname']};charset={$config['dbcharset']}";
        $opt = array(
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        );
        $this->dbh = new PDO($dns, $config['dbuser'], $config['dbpass'], $opt);
    }

    /**
     * Return raw phones from DB
     *
     * @return array
     */
    public function getPhoneNumbers()
    {
        $phones = array();
        foreach ($this->dbh->query("SELECT Phone FROM task1")->fetchAll() as $row) {
            $phones[] = $row['Phone'];
        }
        return $phones;
    }

    /**
     * Formatted phone numbers
     *
     * @param $data
     * @return array
     */
    public function formatPhoneNumbers($data)
    {
        $formattedPhoneNumbers = array();
        foreach($data as $phone) {
            $phone = preg_replace('/[^\d]/', '', $phone);
            if (strlen($phone) < self::PHONE_MIN_LENGTH || strlen($phone) > self::PHONE_MAX_LENGTH) continue;
            $phone = '+' . $phone;
            if (!isset($formattedPhoneNumbers[$phone])) $formattedPhoneNumbers[$phone] = true;
        }
        return array_keys($formattedPhoneNumbers);
    }

    /**
     * Build HTML table based on phone numbers
     *
     * @param $phoneNumbers
     * @return string
     */
    public function buildHtmlTable($phoneNumbers)
    {
        $html = '<table>';
        foreach($phoneNumbers as $phone) {
            $html .= '<tr><td>' . htmlspecialchars($phone) . '</td></tr>';
        }
        $html .= '</table>';

        return $html;
    }
}
