<?php

return [
    'directions' => 'Directions',
    'fromPoint' => 'From Point',
    'toPoint' => 'To Point',
    'getDirections' => 'Get directions',
    'directionsNotFound' => 'Directions not found :(',
];
