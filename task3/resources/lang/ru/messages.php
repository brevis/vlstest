<?php

return [
    'directions' => 'Маршрут',
    'fromPoint' => 'Исходная точка',
    'toPoint' => 'Точка назначения',
    'getDirections' => 'Получить маршрут',
    'directionsNotFound' => 'Маршрут не найден :(',
];
