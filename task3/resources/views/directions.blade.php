<html>
<head>
    <title>Task 3 - Directions</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container">
    <div class="header">
        <nav>
            <ul class="nav nav-pills pull-right">
                <li role="presentation"{!! App::getLocale() == 'en' ? ' class="active"' : '' !!}><a href="{{ route('switchlocale', ['locale' => 'en']) }}">EN</a></li>
                <li role="presentation"{!! App::getLocale() == 'ru' ? ' class="active"' : '' !!}><a href="{{ route('switchlocale', ['locale' => 'ru']) }}">RU</a></li>
            </ul>
        </nav>
        <h3 class="text-muted"><a href="{{ route('homepage') }}">Directions</a></h3>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form action="{{ route('directions') }}" method="post">
                <div class="form-group col-lg-4{{ $errors->has('fromPoint') ? ' has-error' : '' }}">
                    <label for="fromPoint">{{ trans('messages.fromPoint') }}</label>
                    <input type="text" class="form-control" id="fromPoint" name="fromPoint" placeholder="New York, Empire State Building" required value="{{ $fromPoint or '' }}">
                </div>
                <div class="form-group col-lg-4{{ $errors->has('toPoint') ? ' has-error' : '' }}">
                    <label for="toPoint">{{ trans('messages.toPoint') }}</label>
                    <input type="text" class="form-control" id="toPoint" name="toPoint" placeholder="New York, Madison Square Garden" required value="{{ $toPoint or '' }}">
                </div>
                <div class="form-group col-lg-1">
                    <label>&nbsp;</label>
                    <button type="submit" class="btn btn-primary">{{ trans('messages.getDirections') }}</button>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @if (isset($directions))
                <h3>{{ trans('messages.directions') }}:</h3>
                @forelse ($directions as $step)
                    <p>{!! $step !!}</p>
                @empty
                    <p>{{ trans('messages.directionsNotFound') }}</p>
                @endforelse
            @endif
        </div>
    </div>
</div>
</body>
</html>