<?php

namespace app;

class Directions
{
    public static function get($fromPoint, $toPoint, $language='en')
    {
        $response = \GoogleMaps::load('directions')
            ->setParam([
                'origin'        => $fromPoint,
                'destination'   => $toPoint,
                'language'      => $language,
            ])
            ->get();
        $response = json_decode($response);

        if (!is_object($response) || !isset($response->status) || $response->status != 'OK') return [];

        $directions = array();
        foreach ($response->routes[0]->legs[0]->steps as $step) {
            $directions[] = $step->html_instructions;
        }
        return $directions;
    }
}