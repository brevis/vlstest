<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', ['as' => 'homepage', 'uses' => 'DirectionsController@showHomepage']);
    Route::post('/', ['as' => 'directions', 'uses' => 'DirectionsController@showDirections']);
    Route::get('/switchlocale/{locale}', ['as' => 'switchlocale', 'uses' => 'Controller@switchlocale']);
});
