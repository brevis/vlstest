<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Validator;
use \Session;
use \App;
use App\Directions;

class DirectionsController extends Controller
{
    public function showHomepage()
    {
        return view('directions');
    }

    public function showDirections(Request $request)
    {
        $validator = Validator::make($request->request->all(), [
            'fromPoint' => 'required|min:3|max:256',
            'toPoint' => 'required|min:3|max:256'
        ]);

        $fromPoint = $request->request->get('fromPoint');
        $toPoint = $request->request->get('toPoint');

        if ($validator->fails()) {
            return view('directions')
                ->withErrors($validator)
                ->with('fromPoint', $fromPoint)
                ->with('toPoint', $toPoint)
                ;
        }

        $directions = Directions::get($fromPoint, $toPoint, App::getLocale());

        return view('directions')
            ->with('directions', $directions)
            ->with('fromPoint', $fromPoint)
            ->with('toPoint', $toPoint)
            ;
    }

}