<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use \Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct () {
        $lang = Session::get ('locale');
        if ($lang != null) \App::setLocale($lang);
    }

    public function switchlocale($locale)
    {
        if (!in_array($locale, array('en', 'ru'))) $locale = 'en';
        Session::set ('locale', $locale);
        return redirect()->route('homepage');
    }
}
